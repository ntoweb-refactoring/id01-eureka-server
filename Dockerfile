FROM openjdk:8-jdk-alpine
VOLUME [ "/tmp" ]
ARG JAVA_OPTS
ENV JAVA_OPTS=${JAVA_OPTS}
ADD target/*.jar id01-eureka-server.jar
EXPOSE 8761
ENTRYPOINT exec java ${JAVA_OPTS} -jar id01-eureka-server.jar
